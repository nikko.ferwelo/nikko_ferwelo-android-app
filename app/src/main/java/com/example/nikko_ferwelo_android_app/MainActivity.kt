package com.example.nikko_ferwelo_android_app

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.content.Intent

import android.widget.Button
import android.widget.EditText


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button = findViewById<Button>(R.id.button)

        button.setOnClickListener {
            callHealthInfo()
        }
    }

    private fun callHealthInfo() {
        val editTextName = findViewById<EditText>(R.id.editFirstName2)
        val editTextNumber = findViewById<EditText>(R.id.editFirstName)
        val editTextCity = findViewById<EditText>(R.id.editFirstName3)

        val dataName = editTextName.text.toString()
        val dataNumber = editTextNumber.text.toString()
        val dataCity = editTextCity.text.toString()

        val intent = Intent(this, HealthInfo::class.java).also {
            it.putExtra( "NAME_DATA", dataName)
            it.putExtra("NUMBER_DATA", dataNumber)
            it.putExtra("CITY_DATA", dataCity)
            startActivity(it)
        }
    }
}
