package com.example.nikko_ferwelo_android_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
class Result : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val result = intent.getStringExtra("RESULT_DATA")

        val textviewName = findViewById<TextView>(R.id.textView12).apply {
            text = result
        }
    }
}
