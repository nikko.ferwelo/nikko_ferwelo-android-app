package com.example.nikko_ferwelo_android_app

import android.content.Intent
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.nikko_ferwelo_android_app.databinding.ActivityHealthInfoBinding
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView


class HealthInfo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_health_info)

        val button = findViewById<Button>(R.id.button2)
        button.setOnClickListener {
            callConfirmation()
        }

        val dataName = intent.getStringExtra("NAME_DATA")
        val dataNumber = intent.getStringExtra("NUMBER_DATA")
        val dataCity = intent.getStringExtra("CITY_DATA")

    }

    private fun callConfirmation() {
        val tempQ1 = findViewById<TextView>(R.id.textView)
        val tempQ2 = findViewById<TextView>(R.id.textView2)

        val dataName = intent.getStringExtra("NAME_DATA")
        val dataNumber = intent.getStringExtra("NUMBER_DATA")
        val dataCity = intent.getStringExtra("CITY_DATA")
        val dataQ1 = tempQ1.text.toString()
        val dataQ2 = tempQ2.text.toString()

        val intent = Intent(this, Confirmation::class.java).also {
            it.putExtra( "NAME_DATA", dataName)
            it.putExtra("NUMBER_DATA", dataNumber)
            it.putExtra("CITY_DATA", dataCity)
            it.putExtra("Q1_DATA", dataQ1)
            it.putExtra("Q2_DATA", dataQ2)
            startActivity(it)
        }
    }

    fun onRadiobuttonQuestion1Clicked(view: View) {
        val inputSymptoms = findViewById<EditText>(R.id.editTextTextPersonName)
        if (view is RadioButton) {
            val checked = view.isChecked

            when (view.getId()) {
                R.id.radioButton ->
                    if (checked) {
                        val textviewQ1 = findViewById<TextView>(R.id.textView).apply {
                            text = "YES"

                            inputSymptoms.visibility=View.VISIBLE
                        }

                    }
                R.id.radioButton2 ->
                    if (checked) {
                        val textviewQ1 = findViewById<TextView>(R.id.textView).apply {
                            text = "NO"
                            inputSymptoms.visibility=View.INVISIBLE
                        }
                    }
            }
        }

    }

    fun onRadiobuttonQuestion2Clicked(view: View) {
        val inputContact = findViewById<EditText>(R.id.editTextTextPersonName2)
        if (view is RadioButton) {
            val checked = view.isChecked

            when (view.getId()) {
                R.id.radioButton3 ->
                    if (checked) {
                        val textviewQ2 = findViewById<TextView>(R.id.textView2).apply {
                            text = "YES"
                            inputContact.visibility=View.VISIBLE

                        }

                    }
                R.id.radioButton4 ->
                    if (checked) {
                        val textviewQ2 = findViewById<TextView>(R.id.textView2).apply {
                            text = "NO"
                            inputContact.visibility=View.INVISIBLE
                        }
                    }
            }
        }

    }
}
