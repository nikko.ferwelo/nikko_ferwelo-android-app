package com.example.nikko_ferwelo_android_app

import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity

import com.example.nikko_ferwelo_android_app.databinding.ActivityConfirmationBinding
import android.content.Intent


import android.widget.Button
import android.widget.TextView

class Confirmation : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirmation)

        val button = findViewById<Button>(R.id.button3)
        button.setOnClickListener {
            callResult()
        }

        val dataName = intent.getStringExtra("NAME_DATA")
        val dataNumber = intent.getStringExtra("NUMBER_DATA")
        val dataCity = intent.getStringExtra("CITY_DATA")
        val dataQ1 = intent.getStringExtra("Q1_DATA")
        val dataQ2 = intent.getStringExtra("Q2_DATA")

        val textviewName = findViewById<TextView>(R.id.textView5).apply {
            text = dataName
        }
        val textviewNumber = findViewById<TextView>(R.id.textView7).apply {
            text = dataNumber
        }
        val textviewCity = findViewById<TextView>(R.id.textView6).apply {
            text = dataCity
        }

        val textviewQ1 = findViewById<TextView>(R.id.textView8).apply {
            text = dataQ1
        }

        val textviewQ2 = findViewById<TextView>(R.id.textView9).apply {
            text = dataQ2
        }
    }

    private fun callResult() {
        val tempQ1 = findViewById<TextView>(R.id.textView8)
        val tempQ2 = findViewById<TextView>(R.id.textView9)
        val q1 = tempQ1.text.toString()
        val q2 = tempQ2.text.toString()
        if (q1 == "NO" && q2 == "NO") {
            val dataYes = "You are in a good condition, Keep up"
            val intent = Intent(this, Result::class.java).also {
                it.putExtra("RESULT_DATA", dataYes)
                startActivity(it)
            }
        } else {
            val dataNo = "RISKY, you answered a YES, you should isolate yourself!"
            val intent = Intent(this, Result::class.java).also {
                it.putExtra("RESULT_DATA", dataNo)
                startActivity(it)
            }
        }
    }
}
